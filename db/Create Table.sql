

USE DevPMMarlin

IF OBJECT_ID('dbo.MPEPromo_Data') IS NOT NULL
	DROP TABLE dbo.MPEPromo_Data
GO

CREATE TABLE dbo.MPEPromo_Data (
	--ID int IDENTITY(1,1) NOT NULL, -- Using Guid from code
	--ID nvarchar(50) NOT NULL,
	ID uniqueidentifier default newid() NOT NULL,

	StoreNum varchar(6) NOT NULL,
	Wave varchar(4) NOT NULL,

	ItemID varchar(3) NOT NULL,

	Brand_ID varchar(1) NULL,
	Location varchar(1) NULL,

	Pack_Disc varchar(5) NULL,
	Carton_Disc varchar(5) NULL,
	Pack_Sell varchar(5) NULL,
	Carton_Sell varchar(5) NULL,

	NumSigns varchar(5) NULL,

	Tax bit NULL,
	
	UpdateDT datetime NULL
)
GO

--ALTER TABLE dbo.MPEPromo_Data ADD CONSTRAINT [PK_MPEPromo_Data] PRIMARY KEY CLUSTERED ( ID ASC )
ALTER TABLE dbo.MPEPromo_Data ADD CONSTRAINT PK_MPEPromo_Data PRIMARY KEY(ID)
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_MPEPromo_Data_StoreNum] ON dbo.MPEPromo_Data
(
	StoreNum,ItemID ASC
)
GO

select * from MPEPromo_Data
--select * from MPEPromo_Data order by StoreNum,ItemID



