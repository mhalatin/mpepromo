﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MPEPromo.Model;

namespace MPEPromo.Pages.Questionnaire
{
    public class CreateModel : PageModel
    {
        private ApplicationDbContext _db;

        [BindProperty]
        public IList<MPEPromoModel> Sign_Array { get; set; }

        public CreateModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public MPEPromoModel MPEPromo_Data { get; set; }


        public void OnGet()
        {

        }


        public async Task<IActionResult> OnPost()
        {

            if (Sign_Array[0].StoreNum == null || Sign_Array[0].StoreNum.Length != 6)
            {
                ModelState.AddModelError(string.Empty, "Invalid Store Number");
            }
            if (Sign_Array[0].Wave == null || Sign_Array[0].Wave.Length != 4)
            {
                ModelState.AddModelError(string.Empty, "Invalid Wave Number");
            }

            MPEPromo_Data.StoreNum = Sign_Array[0].StoreNum;
            MPEPromo_Data.Wave = Sign_Array[0].Wave;


            if (!ModelState.IsValid)
            {
                return Page();
            }

            bool hasInfo = false;

            for (int i = 0; i <= 14; i++) //14
            {

                //Skip if nothing on line
                if (
                    ( Sign_Array[i].Brand_ID == "0" || Sign_Array[i].Brand_ID == null ) &&
                    (Sign_Array[i].Location == "0" || Sign_Array[i].Location == null) &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].Carton_Disc == null &&
                    Sign_Array[i].Pack_Sell == null &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].NumSigns == null
                    )
                { continue; }

                hasInfo = true;

                MPEPromo_Data.ItemID = GetItemCode(i);

                var dataInDb = _db.MPEPromo_Data.SingleOrDefault(x => x.StoreNum == MPEPromo_Data.StoreNum && x.Wave == MPEPromo_Data.Wave && x.ItemID == MPEPromo_Data.ItemID);
                if (dataInDb != null)
                {
                    ModelState.AddModelError(string.Empty, "Store Number " + MPEPromo_Data.StoreNum + " Sign Type " + MPEPromo_Data.ItemID + " Already Exists in Database");
                }

                //Errors
                if (i >= 0 && i <= 10)
                {
                    if (Sign_Array[i].Brand_ID == "0")
                    {
                        ModelState.AddModelError(string.Empty, "Must have Brand ID on Sign for " + MPEPromo_Data.ItemID);
                    }
                    if (Sign_Array[i].Location == "0")
                    {
                        ModelState.AddModelError(string.Empty, "Must have Location of Sign for " + MPEPromo_Data.ItemID);
                    }
                }
                if ((i >= 0 && i <= 10) || i == 14)
                {
                    if (Sign_Array[i].Pack_Disc == null && Sign_Array[i].Carton_Disc == null)
                    {
                        ModelState.AddModelError(string.Empty, "Must have a Discount Price for " + MPEPromo_Data.ItemID);
                    }
                }
                if ((i >= 0 && i <= 10) || i == 14)
                {
                    if (Sign_Array[i].Pack_Sell == null && Sign_Array[i].Carton_Sell == null)
                    {
                        ModelState.AddModelError(string.Empty, "Must have a Selling Price for " + MPEPromo_Data.ItemID);
                    }
                }
                if (i >= 0 && i <= 10)
                {
                    if (Sign_Array[i].NumSigns == null)
                    {
                        ModelState.AddModelError(string.Empty, "Must have Number of Sign for " + MPEPromo_Data.ItemID);
                    }
                }

                //if (i >= 11 && i <= 13)
                //{
                //    if (Sign_Array[i].Pack_Sell == null)
                //    {
                //        ModelState.AddModelError(string.Empty, "Must have a Selling Pack Price for " + MPEPromo_Data.ItemID);
                //    }
                //}


                if (!ModelState.IsValid)
                {
                    return Page();
                }
            }

            if (hasInfo == false)
            {

                ModelState.AddModelError(string.Empty, "NOTHING ADDED !!");

                if (!ModelState.IsValid)
                {
                    return Page();
                }

            }

            for (int i = 0; i <= 14; i++) //14
            {

                //Skip if nothing on line
                if (
                    (Sign_Array[i].Brand_ID == "0" || Sign_Array[i].Brand_ID == null) &&
                    (Sign_Array[i].Location == "0" || Sign_Array[i].Location == null) &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].Carton_Disc == null &&
                    Sign_Array[i].Pack_Sell == null &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].NumSigns == null
                    )
                { continue; }

                MPEPromo_Data.ItemID = GetItemCode(i);

                MPEPromo_Data.Brand_ID = Sign_Array[i].Brand_ID;
                MPEPromo_Data.Location = Sign_Array[i].Location;
                MPEPromo_Data.Pack_Disc = Sign_Array[i].Pack_Disc;
                MPEPromo_Data.Carton_Disc = Sign_Array[i].Carton_Disc;
                MPEPromo_Data.Pack_Sell = Sign_Array[i].Pack_Sell;
                MPEPromo_Data.Carton_Sell = Sign_Array[i].Carton_Sell;
                MPEPromo_Data.NumSigns = Sign_Array[i].NumSigns;
                MPEPromo_Data.Tax = Sign_Array[i].Tax;

                MPEPromo_Data.UpdateDT = DateTime.Now;

                //generate unique key id field in database without IDENTITY(1,1)
                Guid g;
                g = Guid.NewGuid();
                MPEPromo_Data.ID = g;

                _db.MPEPromo_Data.Add(MPEPromo_Data);

                await _db.SaveChangesAsync();

            }

            return RedirectToPage("/Index");
        }

        //subs
        private string GetItemCode(int i)
        {
            string code;

            if (i == 11) code = "320";
            else if (i == 12) code = "510";
            else if (i == 13) code = "430";
            else if (i == 14) code = "43A";
            else code = (i + 101).ToString();

            return code;
        }
    }
}