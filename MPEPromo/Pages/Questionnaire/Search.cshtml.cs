﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MPEPromo.Model;

namespace MPEPromo.Pages.Questionnaire
{
    public class SearchModel : PageModel
    {
        [DisplayName("Store Number: ")]
        public string storeInput { get; set; }
        [DisplayName("Wave: ")]
        public string waveInput { get; set; }

        private ApplicationDbContext _db;

        public SearchModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public MPEPromoModel MPEPromo_Data { get; set; }
        public IEnumerable<MPEPromoModel> MPEPromo_Records { get; set; }

        public async Task OnGet()
        {
            MPEPromo_Records = await _db.MPEPromo_Data.ToListAsync();
            MPEPromo_Records = MPEPromo_Records.GroupBy(x => new { x.StoreNum,x.Wave }).Select(x => x.FirstOrDefault());
            MPEPromo_Records = MPEPromo_Records.OrderByDescending(x => x.Wave).ThenBy(x => x.StoreNum);
        }

        public async Task<IActionResult> OnPostSearch(string storeInput, string waveInput)
        {

            if ((storeInput != null) && (waveInput != null))
            {
                MPEPromo_Records = _db.MPEPromo_Data.Where(x => x.StoreNum == storeInput && x.Wave == waveInput);
            }
            else if (storeInput != null)
            {
                MPEPromo_Records = _db.MPEPromo_Data.Where(x => x.StoreNum == storeInput);
            }
            else if (waveInput != null)
            {
                MPEPromo_Records = _db.MPEPromo_Data.Where(x => x.Wave == waveInput);
            }
            else
            {
                MPEPromo_Records = await _db.MPEPromo_Data.ToListAsync();
            }

            MPEPromo_Records = MPEPromo_Records.OrderByDescending(x => x.Wave).ThenBy(x => x.StoreNum);

            return Page();

        }
    }
}