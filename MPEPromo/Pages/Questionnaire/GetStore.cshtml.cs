﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MPEPromo.Model;
using Dapper;

namespace MPEPromo.Pages.Questionnaire
{
    public class GetStoreModel : PageModel
    {
        private ApplicationDbContext _db;
        public GetStoreModel(ApplicationDbContext db)
        {
            _db = db;
        }


        [BindProperty]
        public MPEPromoModel MPEPromo_Data { get; set; }

        [BindProperty]
        public IList<MPEPromoModel> Sign_Array { get; set; } = new MPEPromoModel[20].ToList();

        [BindProperty]
        public IEnumerable<MPEPromoModel> MPEPromo_Records { get; set; }
        public IList<MPEPromoModel> Sign_Array2 { get; set; }


        public async Task OnGet(string storeInput, string waveInput)
        {
            MPEPromo_Records = await _db.MPEPromo_Data.ToListAsync();
            MPEPromo_Records = _db.MPEPromo_Data.Where(x => x.StoreNum == storeInput && x.Wave == waveInput).OrderBy(x => x.ItemID);

            Sign_Array2 = MPEPromo_Records.ToList();


            for ( int i = 0; i < Sign_Array2.Count(); i++)
            {
                if ( Sign_Array2[i].ItemID == "101")
                {
                    Sign_Array[0] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "102")
                {
                    Sign_Array[1] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "103")
                {
                    Sign_Array[2] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "104")
                {
                    Sign_Array[3] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "105")
                {
                    Sign_Array[4] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "106")
                {
                    Sign_Array[5] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "107")
                {
                    Sign_Array[6] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "108")
                {
                    Sign_Array[7] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "109")
                {
                    Sign_Array[8] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "110")
                {
                    Sign_Array[9] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "111")
                {
                    Sign_Array[10] = Sign_Array2[i];
                }

                if (Sign_Array2[i].ItemID == "320")
                {
                    Sign_Array[11] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "510")
                {
                    Sign_Array[12] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "430")
                {
                    Sign_Array[13] = Sign_Array2[i];
                }
                if (Sign_Array2[i].ItemID == "43A")
                {
                    Sign_Array[14] = Sign_Array2[i];
                }

            }
        }


        public async Task<IActionResult> OnPost()
        {

            if (Sign_Array[0].StoreNum == null || Sign_Array[0].StoreNum.Length != 6)
            {
                ModelState.AddModelError(string.Empty, "Invalid Store Number");
            }
            if (Sign_Array[0].Wave == null || Sign_Array[0].Wave.Length != 4)
            {
                ModelState.AddModelError(string.Empty, "Invalid Wave Number");
            }

            MPEPromo_Data.StoreNum = Sign_Array[0].StoreNum;
            MPEPromo_Data.Wave = Sign_Array[0].Wave;


            if (!ModelState.IsValid)
            {
                return Page();
            }

            bool hasInfo = false;

            for (int i = 0; i <= 14; i++) //14
            {

                //Skip if nothing on line
                if (
                    (Sign_Array[i].Brand_ID == "0" || Sign_Array[i].Brand_ID == null) &&
                    (Sign_Array[i].Location == "0" || Sign_Array[i].Location == null) &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].Carton_Disc == null &&
                    Sign_Array[i].Pack_Sell == null &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].NumSigns == null
                    )
                { continue; }

                hasInfo = true;

                MPEPromo_Data.ItemID = GetItemCode(i);

                //Errors
                if (i >= 0 && i <= 10)
                {
                    if (Sign_Array[i].Brand_ID == "0")
                    {
                        ModelState.AddModelError(string.Empty, "Must have Brand ID on Sign for " + MPEPromo_Data.ItemID);
                    }
                    if (Sign_Array[i].Location == "0")
                    {
                        ModelState.AddModelError(string.Empty, "Must have Location of Sign for " + MPEPromo_Data.ItemID);
                    }
                }
                if ((i >= 0 && i <= 10) || i == 14)
                {
                    if (Sign_Array[i].Pack_Disc == null && Sign_Array[i].Carton_Disc == null)
                    {
                        ModelState.AddModelError(string.Empty, "Must have a Discount Price for " + MPEPromo_Data.ItemID);
                    }
                }
                if ((i >= 0 && i <= 10) || i == 14)
                {
                    if (Sign_Array[i].Pack_Sell == null && Sign_Array[i].Carton_Sell == null)
                    {
                        ModelState.AddModelError(string.Empty, "Must have a Selling Price for " + MPEPromo_Data.ItemID);
                    }
                }
                if (i >= 0 && i <= 10)
                {
                    if (Sign_Array[i].NumSigns == null)
                    {
                        ModelState.AddModelError(string.Empty, "Must have Number of Sign for " + MPEPromo_Data.ItemID);
                    }
                }

                if (!ModelState.IsValid)
                {
                    return Page();
                }
            }

            if (hasInfo == false)
            {

                ModelState.AddModelError(string.Empty, "No INFO !!");

                if (!ModelState.IsValid)
                {
                    return Page();
                }

            }

            _db.MPEPromo_Data.RemoveRange(_db.MPEPromo_Data.Where(x => x.StoreNum == Sign_Array[0].StoreNum && x.Wave == Sign_Array[0].Wave));
            await _db.SaveChangesAsync();

            for (int i = 0; i <= 14; i++) //14
            {

                //Skip if nothing on line
                if (
                    (Sign_Array[i].Brand_ID == "0" || Sign_Array[i].Brand_ID == null) &&
                    (Sign_Array[i].Location == "0" || Sign_Array[i].Location == null) &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].Carton_Disc == null &&
                    Sign_Array[i].Pack_Sell == null &&
                    Sign_Array[i].Pack_Disc == null &&
                    Sign_Array[i].NumSigns == null
                    )
                { continue; }

                MPEPromo_Data.ItemID = GetItemCode(i);

                MPEPromo_Data.Brand_ID = Sign_Array[i].Brand_ID;
                MPEPromo_Data.Location = Sign_Array[i].Location;
                MPEPromo_Data.Pack_Disc = Sign_Array[i].Pack_Disc;
                MPEPromo_Data.Carton_Disc = Sign_Array[i].Carton_Disc;
                MPEPromo_Data.Pack_Sell = Sign_Array[i].Pack_Sell;
                MPEPromo_Data.Carton_Sell = Sign_Array[i].Carton_Sell;
                MPEPromo_Data.NumSigns = Sign_Array[i].NumSigns;
                MPEPromo_Data.Tax = Sign_Array[i].Tax;

                MPEPromo_Data.UpdateDT = DateTime.Now;

                //generate unique key id field in database without IDENTITY(1,1)
                Guid g;
                g = Guid.NewGuid();
                MPEPromo_Data.ID = g;

                _db.MPEPromo_Data.Add(MPEPromo_Data);

                await _db.SaveChangesAsync();

            }

            return RedirectToPage("/Index");
        }

        public async Task<IActionResult> OnPostDelete()
        {
            _db.MPEPromo_Data.RemoveRange(_db.MPEPromo_Data.Where(x => x.StoreNum == Sign_Array[0].StoreNum && x.Wave == Sign_Array[0].Wave));
            await _db.SaveChangesAsync();

            return RedirectToPage("/Index");
        }

        //subs
        private string GetItemCode(int i)
        {
            string code;

            if (i == 11) code = "320";
            else if (i == 12) code = "510";
            else if (i == 13) code = "430";
            else if (i == 14) code = "43A";
            else code = (i + 101).ToString();

            return code;
        }


    }
}