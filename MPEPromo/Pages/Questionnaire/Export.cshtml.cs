﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MPEPromo.Model;

namespace MPEPromo.Pages.Questionnaire
{
    public class ExportModel : PageModel
    {
        [BindProperty]
        [DisplayName("Enter Wave to Export: ")]
        [Required(ErrorMessage = "Wave Number is required.")]
        [MinLength(4)]
        public string waveInput { get; set; }

        private ApplicationDbContext _db;

        public ExportModel(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<MPEPromoModel> MPEPromo_Records { get; set; }

        public void OnGet()
        {

        }

        public IActionResult OnPostExport(string waveInput)
        {
            if (waveInput == null)
            {
                ModelState.AddModelError("waveInput", "Wave Number is required.");
                return Page();
            }

            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (waveInput != null)
            {
                MPEPromo_Records = _db.MPEPromo_Data.Where(x => x.Wave == waveInput);

                string FileName = "MPEPromo_Data" + waveInput + ".txt";

                //Write Data to File

                //StreamWriter file = new StreamWriter(@"\\CAP12DC01\Shared\MPEPromo\MPEPromo_Data" + waveInput + ".txt");
                StreamWriter file = new StreamWriter(@"\\CAP12DC01\Shared\MPEPromo\" + FileName);

                foreach (MPEPromoModel line in MPEPromo_Records)
                {


                    file.WriteLine(
                        "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}",
                        line.StoreNum,
                        line.Wave,
                        line.ItemID,
                        line.Brand_ID,
                        line.Location,
                        line.Pack_Disc,
                        line.Carton_Disc,
                        line.Pack_Sell,
                        line.Carton_Sell,
                        line.NumSigns,
                        Convert.ToInt16(line.Tax)
 
                        );
                }

                file.Close();

                SendMail(FileName);

                return RedirectToPage("/Index");
            }

            return Page();


        }

        private void SendMail(string FileName)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.To.Add("mhalatin@capstoneresearch.com");
                //mailMessage.To.Add("dpriff@capstoneresearch.com");
                //mailMessage.To.Add("tsperger@capstoneresearch.com");
                mailMessage.From = new MailAddress("mhalatin@capstoneresearch.com");
                mailMessage.Subject = "MPEPromo Data Export e-mail";
                mailMessage.Body = "s:/MPEPromo/" + FileName + " Exported!";
                SmtpClient smtpClient = new SmtpClient("CAP12EXCH01.CAPSTONE.local");
                smtpClient.Send(mailMessage);
                Console.Write("Data Exported!");

            }
            catch (Exception ex)
            {
                Console.Write("Could not export data - error: " + ex.Message);

            }
        }
    }
}