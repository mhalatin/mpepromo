﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MPEPromo.Model
{
    public class MPEPromoModel
    {

        [Key]
        public Guid ID { get; set; }

        public string StoreNum { get; set; }
        public string Wave { get; set; }

        public string ItemID { get; set; }

        public string Brand_ID { get; set; }
        public string Location { get; set; }

        public string Pack_Disc { get; set; }
        public string Carton_Disc { get; set; }
        public string Pack_Sell { get; set; }
        public string Carton_Sell { get; set; }

        public string NumSigns { get; set; }

        public bool Tax { get; set; }

        public DateTime UpdateDT { get; set; }
    }
}
