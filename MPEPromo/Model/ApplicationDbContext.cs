﻿using Microsoft.EntityFrameworkCore;

namespace MPEPromo.Model
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<MPEPromoModel> MPEPromo_Data { get; set; }
    }


}
